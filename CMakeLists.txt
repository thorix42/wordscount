cmake_minimum_required (VERSION 3.16)

set(CMAKE_CXX_STANDARD 20)

project(wordsCount)

include_directories("${PROJECT_SOURCE_DIR}")

 
# add the executable
add_executable(wordsCount
    main.cpp
    WordsCount.cpp

    WordsCount.h
)

