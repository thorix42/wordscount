#include "WordsCount.h"

#include <algorithm>
#include <codecvt>
#include <fstream>
#include <iostream>
#include <iterator>
#include <locale>
#include <sstream>

std::map<wchar_t, int> WordsCount::s_lettersAndScores;

WordsCount::WordsCount()
{
	if (s_lettersAndScores.empty())
	{
		s_lettersAndScores[L'A'] = 32;
		s_lettersAndScores[L'B'] = 36;
		s_lettersAndScores[L'C'] = 33;
		s_lettersAndScores[L'D'] = 40;
		s_lettersAndScores[L'E'] = 41;
		s_lettersAndScores[L'F'] = 47;
		s_lettersAndScores[L'G'] = 31;
		s_lettersAndScores[L'H'] = 27;
		s_lettersAndScores[L'I'] = 49;
		s_lettersAndScores[L'J'] = 28;
		s_lettersAndScores[L'K'] = 30;
		s_lettersAndScores[L'L'] = 42;
		s_lettersAndScores[L'M'] = 29;
		s_lettersAndScores[L'N'] = 38;
		s_lettersAndScores[L'O'] = 51;
		s_lettersAndScores[L'P'] = 43;
		s_lettersAndScores[L'Q'] = 45;
		s_lettersAndScores[L'R'] = 39;
		s_lettersAndScores[L'S'] = 35;
		s_lettersAndScores[L'T'] = 52;
		s_lettersAndScores[L'U'] = 37;
		s_lettersAndScores[L'V'] = 46;
		s_lettersAndScores[L'W'] = 34;
		s_lettersAndScores[L'X'] = 48;
		s_lettersAndScores[L'Y'] = 44;
		s_lettersAndScores[L'Z'] = 50;
		s_lettersAndScores[L'\xe9'] = 60; // �
		s_lettersAndScores[L'\xe8'] = 61; // �
		s_lettersAndScores[L'\xea'] = 62; // �
		s_lettersAndScores[L'\xe0'] = 63; // �
		s_lettersAndScores[L'\xe2'] = 64; // �
		s_lettersAndScores[L'\xeb'] = 65; // �
		s_lettersAndScores[L'\xfb'] = 66; // �
		s_lettersAndScores[L'\xf9'] = 67; // �
		s_lettersAndScores[L'\xee'] = 68; // �
		s_lettersAndScores[L'\xe7'] = 69; // �
		s_lettersAndScores[L'\xf4'] = 70; // �
		s_lettersAndScores[L'\xf6'] = 71; // �
		s_lettersAndScores[L'\xfc'] = 72; // �
		s_lettersAndScores[L'a'] = 1;
		s_lettersAndScores[L'b'] = 4;
		s_lettersAndScores[L'c'] = 5;
		s_lettersAndScores[L'd'] = 8;
		s_lettersAndScores[L'e'] = 10;
		s_lettersAndScores[L'f'] = 11;
		s_lettersAndScores[L'g'] = 13;
		s_lettersAndScores[L'h'] = 16;
		s_lettersAndScores[L'i'] = 18;
		s_lettersAndScores[L'j'] = 19;
		s_lettersAndScores[L'k'] = 21;
		s_lettersAndScores[L'l'] = 21;
		s_lettersAndScores[L'm'] = 23;
		s_lettersAndScores[L'n'] = 2;
		s_lettersAndScores[L'o'] = 3;
		s_lettersAndScores[L'p'] = 6;
		s_lettersAndScores[L'q'] = 7;
		s_lettersAndScores[L'r'] = 9;
		s_lettersAndScores[L's'] = 12;
		s_lettersAndScores[L't'] = 14;
		s_lettersAndScores[L'u'] = 15;
		s_lettersAndScores[L'v'] = 17;
		s_lettersAndScores[L'w'] = 20;
		s_lettersAndScores[L'x'] = 24;
		s_lettersAndScores[L'y'] = 25;
		s_lettersAndScores[L'z'] = 26;
	}
}

void WordsCount::setWords(const std::set<std::wstring>& words)
{
	m_wordsAndScores.clear();
	for (auto& word: words)
	{
		int score = 0;
		for (auto& letter : word)
		{
			score += s_lettersAndScores[letter];
		}
		m_wordsAndScores.push_back(std::pair<std::wstring, int>(word, score));
	}
	std::sort(m_wordsAndScores.begin(), m_wordsAndScores.end(),
		[](const std::pair<std::wstring, int>& a, const std::pair<std::wstring, int>& b) { return a.second < b.second; });
}

std::vector<std::pair<std::wstring, int>> WordsCount::getWords() const
{
	return m_wordsAndScores;
}

void WordsCount::write(const std::filesystem::path& filename)
{
	if (getWords().size() == 0)
	{
		std::cout << "No word to print, do not write file." << std::endl;
		return;
	}

	std::basic_ofstream<wchar_t> fout(filename);
	fout.imbue(std::locale(std::locale(), new std::codecvt_utf8<wchar_t>));
	if (!fout.is_open())
	{
		std::cout << "File " << filename << " cannot be opened." << std::endl;
		return;
	}

	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
	for (auto& [word, score] : getWords())
	{
		fout << word << L", " << converter.from_bytes(std::to_string(score)) << std::endl;
	}
}

WordsCount WordsCount::read(const std::filesystem::path& filename)
{
	std::basic_ifstream<wchar_t> fin(filename);
	fin.imbue(std::locale(std::locale(), new std::codecvt_utf8<wchar_t>));
	if (!fin.is_open())
	{
		std::cout << "File " << filename << " cannot be opened." << std::endl;
		return WordsCount();
	}

	std::wstring str((std::istreambuf_iterator<wchar_t>(fin)), (std::istreambuf_iterator<wchar_t>()));


	// Not clear what is supposed to be a word separator. Cleanup list to replace
	// word separator with \n.
	// Maybe use a regexp
	std::replace(str.begin(), str.end(), L' ', L'\n');
	std::replace(str.begin(), str.end(), L',', L'\n');
	std::replace(str.begin(), str.end(), L'\x2019', L'\n'); // �
	std::replace(str.begin(), str.end(), L'+', L'\n');

	std::basic_stringstream<wchar_t> sstr(str);

	std::set<std::wstring> words;
	std::wstring word;
	try
	{
		while (std::getline(sstr, word))
		{
			if (!word.empty())
				words.insert(word);
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "File " << filename << " cannot be readed: " << e.what() << std::endl;
	}

	WordsCount wordsCount;
	wordsCount.setWords(words);
	return wordsCount;
}
