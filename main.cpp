#include <codecvt>
#include <filesystem>
#include <iostream>
#include <locale>

#include "WordsCount.h"

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cout << "Usage: wordsCount <input filename>" << std::endl;
        return 0;
    }
    std::filesystem::path inputFilename = argv[1];
    WordsCount wordsCount = WordsCount::read(inputFilename);

    std::filesystem::path outputFilename = inputFilename.parent_path() / (inputFilename.stem().string() + ".count" + inputFilename.extension().string());
    wordsCount.write(outputFilename);

    return 0;
}
