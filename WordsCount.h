#pragma once

#include <filesystem>
#include <map>
#include <set>
#include <string>
#include <vector>

class WordsCount
{
public:
    WordsCount();
    ~WordsCount() = default;

    // Set words and remove duplicate ones
    void setWords(const std::set<std::wstring>& words);

    // Return words and scores
    std::vector<std::pair<std::wstring, int>> getWords() const;

    // Write WordsCount in file.
    // If there is no word to write, file is not created.
    void write(const std::filesystem::path& filename);

    // Create WordsCount from word in file.
    // If file cannot be read, return a WordCount object without word and print message in cout.
    static WordsCount read(const std::filesystem::path& filename);

private:
    std::vector<std::pair<std::wstring, int>> m_wordsAndScores;
    static std::map<wchar_t, int> s_lettersAndScores;

};
